package com.dcastalia.android.digitalcatalogue;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * The Adapter is also responsible for making a View for each item in the data set.Adapters are basically used to deliver content.
 */
public class SpinnerDialogArrayAdapter extends ArrayAdapter<String>{
	private final Context context;
	private final String[] values;
	private float rx,ry;
	Global global;
	public SpinnerDialogArrayAdapter(Context context, String values[]) {
		super(context, R.layout.spinerlayout,values);
		this.context = context;
		this.values = values;
		global = new Global();
		rx = global.getRx();
		ry = global.getRy();
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.spinerlayout, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.spinner_label);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.spinner_button);

		textView.setText(values[position]);

		/*textView.setTextSize((16f*ry)/global.f);
		textView.setGravity(Gravity.CENTER_VERTICAL);
		LayoutParams imageView_paarams = new LayoutParams((int)(32*rx),(int)(32*ry));
		imageView_paarams.setMargins((int)(4*rx), (int)(4*ry), (int)(10*rx), 0);
		imageView.setLayoutParams(imageView_paarams);*/

		return rowView;
	}
}
