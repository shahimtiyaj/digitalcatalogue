package com.dcastalia.android.digitalcatalogue;

/**
 * Table is Data access object class which the Table name
 */

public class Table {

    String name;
    
    public Table(String uname){
        this.name = uname;
    }
    
    public String getTableName(){
        return name;
    }
}
