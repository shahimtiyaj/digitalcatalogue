package com.dcastalia.android.digitalcatalogue;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MySimpleArrayAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] values;
    Global global;
    private float rx, ry;
    private String nowHere;

    public MySimpleArrayAdapter(Context context, String[] values, String nowHere) {
        super(context, R.layout.rowlayout, values);
        this.context = context;
        this.values = values;
        global = new Global();
        rx = global.getRx();
        ry = global.getRy();

        this.nowHere = nowHere;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rowlayout, parent, false);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

        if(position == 0)
        imageView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.starter_01));
        else if(position == 1)
            imageView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.maindish_02));
        else if(position == 2)
            imageView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.dessert_03));
        else if(position == 3)
            imageView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.beverages_04));

        TextView textView = (TextView) rowView.findViewById(R.id.label);
        textView.setText(values[position]);

        if(nowHere.trim().equals(values[position].trim())){
            LinearLayout linearLayout = (LinearLayout) rowView.findViewById(R.id.linearLayout_row_layout_main);
            linearLayout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.container_bg));
        }
        //textView.setTextColor(Color.BLACK);
        //textView.setTextSize((20f * ry) / global.f);
        //textView.setGravity(Gravity.CENTER);
        //LayoutParams imageView_paarams = new LayoutParams((int) (32 * rx), (int) (32 * ry));
        //imageView_paarams.setMargins((int) (4 * rx), (int) (4 * ry), (int) (10 * rx), 0);
        //imageView.setLayoutParams(imageView_paarams);
        return rowView;
    }
}

