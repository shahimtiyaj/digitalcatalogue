package com.dcastalia.android.digitalcatalogue;

/**
 * RestPhoto is the data access object(DAO ) or model class which hold the restaurant photo or image url
 */
public class RestPhoto {

	String ImageUrl;
	
	public RestPhoto(String url){
		this.ImageUrl = url;
	}
	
	public String getImage_Url(){
		return ImageUrl;
	}
}
