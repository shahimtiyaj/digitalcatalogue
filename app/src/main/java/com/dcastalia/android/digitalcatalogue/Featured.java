package com.dcastalia.android.digitalcatalogue;

import java.util.ArrayList;

/**
 *  Featured is the Data access object or  model class(DAO) that hold the data
 */

public class Featured {

    //Initialization the variable
    String id;
    String name;
    String image;
    String desc;
    String price;
    String unit;

    /**
     *
     * @param id is used for keeping the Featured id
     * @param name item name
     * @param image item image
     * @param desc item description
     * @param price item price
     * @param unit item name
     */

    public Featured(String id, String name, String image, String desc, String price, String unit){
        this.id = id;
        this.name = name;
        this.image = image;
        this.desc = desc;
        this.price = price;
        this.unit = unit;
    }
    
    ArrayList<Subitem>subitem = new ArrayList<Subitem>();

    /**
     *
     * @param id sub item id
     * @param name subitem name
     * @param price subitem price
     * @param unit subitem unit
     */

    public void setSubItem(String id, String name, String price, String unit){
        subitem.add(new Subitem(id, name, price, unit));
    }
    
    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getImage(){
        return image;
    }

    public String getDesc(){
        return desc;
    }

    public String getPrice(){
        return price;
    }

    public String getUnit(){
        return unit;
    }
}
