package com.dcastalia.android.digitalcatalogue;

import android.app.Activity;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Model class is known as data access object (DAO)
 */

public class Global {
    public static float f;
    public static int ListId;
    //public static String ListName;
    public static int MenuId;
    public static int ItemId;
    public static int FeatureId;
    public static int SubitemId;
    public static OptionsParser opt_parse;
    public static ListCategoryParser Listparser;
    public static MenuParser Menuparser;
    public static DataHelper dh;
    public static String bckt = "";
    public static int count = 0;
    public static boolean FeaturedItemClicked = false;
    public static String Tobacco = "";
    public static double vat;
    public static double service;
    public static int spinnerposition;
    public static String age_str = "";
    public static String occupation_str = "";
    public static String WAITER_ID = "TAP TO SELECT";
    public static String TABLE_ID = "TAP TO SELECT";
    public static Activity menu_activity;
    public static ArrayList<Integer> item_amount = new ArrayList<Integer>();
    public static ArrayList<Double> item_price = new ArrayList<Double>();
    public static ArrayList<String> item_name = new ArrayList<String>();
    public static ArrayList<Tobacco> TobaccoList = new ArrayList<Tobacco>();
    public static float rx;
    public static float ry;

    public static String restaurantLogo = "";

    /*
    public static final String optionsXML = "http://dcastalia.com/projects/web/fms/file/options.xml";
    public static final String meal_typeXML = "http://dcastalia.com/projects/web/fms/file/meal_type.xml";
    public static final String order_tableUrl = "http://dcastalia.com/projects/web/fms/index.php/android/save";
    public static final String form_tableURL = "http://dcastalia.com/projects/web/fms/index.php/android/save_client_details";
    */

    public static String optionsXML = "";
    public static String meal_typeXML = "";
    public static String order_tableUrl = "";
    public static String form_tableURL = "";


    public static boolean isOnline;

    public void setOrder_tableUrl(String orderTableURL) {
        order_tableUrl = orderTableURL;
    }

    public String getForm_tableURL() {
        return form_tableURL;
    }

    public void setForm_tableURL(String formtableURL) {
        form_tableURL = formtableURL;
    }

    public String getOrder_tableURL() {
        return order_tableUrl;
    }

    public String getMeal_typeXML() {
        return meal_typeXML;
    }

    public void setMeal_typeXML(String mealTypeXML) {
        meal_typeXML = mealTypeXML;
    }

    public String getOptionsXML() {
        return optionsXML;
    }

    public void setOptionsXML(String optionXML) {
        optionsXML = optionXML;
    }

    public double getVat() {
        return vat;
    }

    public void setVat(double d) {
        vat = d;
    }

    public double getService() {
        return service;
    }

    public void setService(double d) {
        service = d;
    }

    public void addItem_amount(int amount) {
        item_amount.add(amount);
    }

    public ArrayList<Integer> getItem_amount() {
        return item_amount;
    }

    public void addItem_price(Double price) {
        item_price.add(price);
    }

    public ArrayList<Double> getItem_price() {
        return item_price;
    }

    public void addItem_name(String name) {
        item_name.add(name);
    }

    public ArrayList<String> getItem_name() {
        return item_name;
    }

    public int getFeatureId() {
        return FeatureId;
    }

    public void setFeatureId(int id) {
        FeatureId = id;
    }

    public boolean getFeaturedItemClicked() {
        return FeaturedItemClicked;
    }

    public void setFeaturedItemClicked(boolean b) {
        FeaturedItemClicked = b;
    }

    public OptionsParser getOptionsparser() {
        return opt_parse;
    }

    public void setOptionsparser(OptionsParser opt) {
        opt_parse = opt;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int i) {
        count = i;
    }

    public String getBckt() {
        return bckt;
    }

    public void setBckt(String s) {
        bckt += s;
    }

    public void updateBckt(String s) {
        bckt = s;
    }

    /**
     * Clear the data from database
     */
    public void clearBckt() {
        item_amount = new ArrayList<Integer>();
        item_price = new ArrayList<Double>();
        item_name = new ArrayList<String>();
    }

    public DataHelper getDatahelper() {
        return dh;
    }

    public void setDatahelper(DataHelper datahelper) {
        dh = datahelper;
    }

    public MenuParser getMenuparser() {
        return Menuparser;
    }

    public void setMenuparser(MenuParser parser) {
        Menuparser = parser;
    }

    public ListCategoryParser getListparser() {
        return Listparser;
    }

    public void setListparser(ListCategoryParser parser) {
        Listparser = parser;
    }

    public int getListId() {
        return ListId;
    }

    // List>Menu>Item>SubItem
    public void setListId(int id) {
        ListId = id;
    }

    public int getMenuId() {
        return MenuId;
    }

    public void setMenuId(int id) {
        MenuId = id;
    }

    public int getItemId() {
        return ItemId;
    }

    public void setItemId(int id) {
        ItemId = id;
    }

    public int getSubitemId() {
        return SubitemId;
    }

    public void setSubitemId(int id) {
        SubitemId = id;
    }

    public int getSpinnerposition() {
        return spinnerposition;
    }

    public void setSpinnerposition(int position) {
        spinnerposition = position;
    }

    public String getAge_str() {
        return age_str;
    }

    public void setAge_str(String s) {
        age_str = s;
    }

    public String getOccupation_str() {
        return occupation_str;
    }

    public void setOccupation_str(String s) {
        occupation_str = s;
    }

    public String getWAITER_ID() {
        return WAITER_ID;
    }

    public void setWAITER_ID(String id) {
        WAITER_ID = id;
    }

    public String getTABLE_ID() {
        return TABLE_ID;
    }

    public void setTABLE_ID(String id) {
        TABLE_ID = id;
    }

    public Activity getMenu_ctx() {
        return menu_activity;
    }

    public void setMenu_ctx(Activity c) {
        menu_activity = c;
    }

    public String getIsTobacco() {
        return Tobacco;
    }

    public void setIsTobacco(String bool) {
        Tobacco = bool;
    }

    public ArrayList<Tobacco> getTobaccoList() {
        return TobaccoList;
    }

    public void setTobaccoList(ArrayList<Tobacco> List) {
        TobaccoList = List;
    }

    public float getRx() {
        return rx;
    }

    public void setRx(float r) {
        rx = r;
    }

    public float getRy() {
        return ry;
    }

    public void setRy(float r) {
        ry = r;
    }

    /**
     * To convert any string to modified Camel case or Title case
     * @param s is used for receiving a sentence than converting the proper case
     * @return modified Camel case or Title case
     */
    public static String toCamelCaseSentence(String s) {

        if (s != null) {
            String[] words = s.split(" ");

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < words.length; i++) {
                sb.append(toCamelCaseWord(words[i]));
            }

            return sb.toString().trim();
        } else {
            return "";
        }
    }

    /**
     *Converting a String to Proper Case
     * @param word is receiving and converting in the proper case
     * @return the modified word
     */

    public static String toCamelCaseWord(String word) {
        if (word ==null){
            return "";
        }

        switch (word.length()) {
            case 0:
                return "";
            case 1:
                return word.toUpperCase(Locale.getDefault()) + " ";
            default:
                char firstLetter = Character.toUpperCase(word.charAt(0));
                return firstLetter + word.substring(1).toLowerCase(Locale.getDefault()) + " ";
        }
    }

}
