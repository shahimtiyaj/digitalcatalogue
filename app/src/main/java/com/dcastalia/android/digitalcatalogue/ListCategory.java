package com.dcastalia.android.digitalcatalogue;

/**
 * ListCtegory is the data access object (DAO) or  model class that hold the data
 */

public class ListCategory {

	String id;
	String name;
	String fileName;
	String image;

	/**
	 *
	 * @param id ListCategory Id
	 * @param name List Category name
	 * @param fileName ListCategory file name
     * @param image ListCategory Image
     */
	public ListCategory(String id, String name, String fileName,String image){
		this.id = id;
		this.name = name;
		this.fileName = fileName;
		this.image = image;
	}
	
	public String getImage(){
		return image;
	}
	
	public String getId(){
		return id;
	}
	
	public String getName(){
		return name;
	}
	
	public String getFileName(){
		return fileName;
	}
}
