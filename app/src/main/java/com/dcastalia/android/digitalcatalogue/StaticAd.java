package com.dcastalia.android.digitalcatalogue;

/**
 * StaticAd is data access object or model class which hold the data of StaticAd
 */

public class StaticAd {
    String id;
    String title;
    String desc;
    String link;
    String order;
    String image;

    /**
     *
     * @param id Ad id
     * @param title Ad title
     * @param desc Ad description
     * @param link Ad link
     * @param order
     * @param image
     */
    
    public StaticAd(String id, String title, String desc, String link, String order, String image){
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.link = link;
        this.order = order;
        this.image = image;
    }
    
}
