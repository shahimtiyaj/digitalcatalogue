package com.dcastalia.android.digitalcatalogue;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.image.SmartImageView;

/**
 *
 */
public class LoginActivity extends Activity {

    EditText username, password;
    Button login_button;

    String user = "TAP TO SELECT";
    String pass = "TAP TO SELECT";

    Global global;

    int width, height;

    Display mDisplay;

    SmartImageView logoView;

    TextView open_time, close_time, r_name, r_desc;

    OptionsParser OP;

    String optionsUrl;

    DataHelper dh;

    float rx, ry;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login);

        global = new Global();

        setGlobalRatio();
        setElement();

        username = (EditText) findViewById(R.id.userName);
        username.setHint("Username");

        password = (EditText) findViewById(R.id.password);
        password.setHint("Domain Ex: dcastalia.com/projects/web");
    }

    public void setGlobalRatio() {
        mDisplay = getWindowManager().getDefaultDisplay();

        width = mDisplay.getWidth();
        height = mDisplay.getHeight();

        //Toast.makeText(this, width+" "+height, 10).show();

        global.setRx(width / 800f);
        global.setRy(height / 480f);

        rx = global.getRx(); //Local RX
        ry = global.getRy(); //Local RY

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int screenDensity = metrics.densityDpi;

        global.f = screenDensity / 160f;
    }

    /**
     * Set the view containers.Initialize the xml contents.Specially design related
     */
    public void setElement() {
        LinearLayout parent_layout = (LinearLayout) findViewById(R.id.parent_layout);
        //parent_layout.setPadding(0, (int) (10 * ry), 0, 0);

        LinearLayout header_layout = (LinearLayout) findViewById(R.id.header_layout);
        /*LinearLayout.LayoutParams header_layout_params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT, (int) (64 * ry));
        header_layout.setLayoutParams(header_layout_params);*/

        logoView = (SmartImageView) findViewById(R.id.ImgView);
        /*LinearLayout.LayoutParams logo_layout_params = new LinearLayout.LayoutParams((int) (64 * rx), (int) (64 * ry));
        logo_layout_params.setMargins((int) (20 * rx), 0, 0, 0);
        logoView.setLayoutParams(logo_layout_params);*/

        LinearLayout background_layout = (LinearLayout) findViewById(R.id.background_layout);
        //background_layout.setPadding((int) (50 * rx), (int) (30 * ry), (int) (30 * rx), (int) (30 * ry));

        LinearLayout body_layout = (LinearLayout) findViewById(R.id.body_layout);
        //body_layout.setPadding((int) (180 * rx), (int) (20 * ry), 0, 0);

        //body_layout.setGravity(Gravity.CENTER);

        LinearLayout cont_layout = (LinearLayout) findViewById(R.id.container_layout);
        /*LinearLayout.LayoutParams cont_layout_params = new LinearLayout.LayoutParams(
                (int) (400 * rx), LinearLayout.LayoutParams.WRAP_CONTENT);
        cont_layout.setLayoutParams(cont_layout_params);
        cont_layout.setPadding((int) (5 * rx), (int) (10 * ry), (int) (5 * rx), (int) (5 * ry));*/

        LinearLayout inputborderlayout1 = (LinearLayout) findViewById(R.id.input_borderlayout1);
        //inputborderlayout1.setPadding((int) (10 * rx), (int) (10 * ry), (int) (10 * rx), (int) (10 * ry));
        LinearLayout inputborderlayout2 = (LinearLayout) findViewById(R.id.input_borderlayout2);
        //inputborderlayout2.setPadding((int) (10 * rx), (int) (10 * ry), (int) (10 * rx), (int) (10 * ry));

        TextView usernameText = (TextView) findViewById(R.id.uname_text);
        usernameText.setText("Username");
        //usernameText.setTextSize((22f * ry) / global.f);

        TextView passwordText = (TextView) findViewById(R.id.pass_text);
        passwordText.setText("Domain");
        //passwordText.setTextSize((22f * ry) / global.f);

        login_button = (Button) findViewById(R.id.login_button);
        login_button.setOnClickListener(login_listener);
        /*login_button.setLayoutParams(new LinearLayout.LayoutParams((int) (70 * rx), (int) (30 * ry)));
        login_button.setTextSize((15f * ry) / global.f);*/

        /*TextView powerText = (TextView) findViewById(R.id.power_textView);
        powerText.setTextSize((12f * ry) / global.f);

        ImageView frraView = (ImageView) findViewById(R.id.ferra_imageView);
        LinearLayout.LayoutParams ferra_params = new LinearLayout.LayoutParams((int) (100 * rx), (int) (50 * ry));
        ferra_params.setMargins(0, 0, (int) (80 * rx), 0);
        frraView.setLayoutParams(ferra_params);*/

        r_name = (TextView) findViewById(R.id.r_name);
        r_name.setText(getResources().getString(R.string.app_name));
        //r_name.setTextSize((32f * ry) / global.f);

        r_desc = (TextView) findViewById(R.id.r_desc);
        r_desc.setText("");
        //r_desc.setTextSize((12f * ry) / global.f);

        LinearLayout settings_button_linearLinearLayout = (LinearLayout) findViewById(R.id.settings_button_linearLayout);
        settings_button_linearLinearLayout.setVisibility(View.GONE);

        Button settings_button = (Button) findViewById(R.id.settings);
        settings_button.setVisibility(View.GONE);

        Button close_button = (Button) findViewById(R.id.close);
        close_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
    }

    public View.OnClickListener login_listener = new View.OnClickListener() {

        public void onClick(View v) {
            if (username.getText().toString().equals("") || (password.getText().toString().equals(""))) {
                Toast.makeText(LoginActivity.this, "Cannot be empty", Toast.LENGTH_LONG).show();
            } else {

                if (username.getText().toString().equals("fms")  || username.getText().toString().equals("testebud") || username.getText().toString().equals("tastebud") || true) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("LoginSession", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();

                    editor.putString("username", username.getText().toString());
                    editor.putString("password", password.getText().toString());
                    editor.putString("domain", password.getText().toString());
                    editor.putBoolean("logged_in", true);

                    editor.commit(); // commit changes

//                    global.setOptionsXML("http://"+ password.getText().toString() +"/projects/web/" + username.getText().toString() + "/file/options.xml");
//                    global.setMeal_typeXML("http://"+ password.getText().toString() +"/projects/web/" + username.getText().toString() + "/file/meal_type.xml");
//                    global.setOrder_tableUrl("http://"+ password.getText().toString() +"/projects/web/" + username.getText().toString() + "/index.php/android/save");
//                    global.setForm_tableURL("http://"+ password.getText().toString() +"/projects/web/" + username.getText().toString() + "/index.php/android/save_client_details");

                    global.setOptionsXML("http://"+ password.getText().toString() +"/" + username.getText().toString() + "/file/options.xml");
                    global.setMeal_typeXML("http://"+ password.getText().toString() +"/" + username.getText().toString() + "/file/meal_type.xml");
                    global.setOrder_tableUrl("http://"+ password.getText().toString() +"/" + username.getText().toString() + "/index.php/android/save");
                    global.setForm_tableURL("http://"+ password.getText().toString() +"/" + username.getText().toString() + "/index.php/android/save_client_details");

                    startActivity(new Intent(LoginActivity.this, FoodMenu.class));
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, "Wrong Authentication", Toast.LENGTH_LONG).show();
                }
            }
        }
    };
}
