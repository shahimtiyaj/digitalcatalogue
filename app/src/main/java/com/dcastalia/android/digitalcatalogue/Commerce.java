package com.dcastalia.android.digitalcatalogue;

/**
 * Commerce is model class or data access object (DAO) that hold the data
 */



public class Commerce {

    String vat;
    String service;

    /**
     *
     * @param vat
     * @param service
     */
    public Commerce(String vat, String service){
        this.vat = vat;
        this.service = service;
    }
    
    public String getVat(){
        return vat;
    }
    
    public String getService(){
        return service;
    }
}
