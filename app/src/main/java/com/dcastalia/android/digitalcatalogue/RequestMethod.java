package com.dcastalia.android.digitalcatalogue;

public enum RequestMethod {
	GET,
	POST,
}
