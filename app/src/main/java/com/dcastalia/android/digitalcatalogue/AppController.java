package com.dcastalia.android.digitalcatalogue;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Aplication controller is control unit of total application it executes when run the apps.
 */

public class AppController extends Application {

    public static final String TAG = AppController.class.getSimpleName();

    private static AppController mInstance;

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }
}
