package com.dcastalia.android.digitalcatalogue;

/**
 * Tabacco is Data access object class which hold the food id and food name etc.
 */

public class Tobacco {

	int id_food;
	String food_name;
	int id_cat;
	int amount;
	String desc;
	Double TotalItemPrice;//subitem incl
	Double FoodPrice;
	Double TotalPrice;//amount*totalitemprice
	int rowId;

    /**
     *
     * @param rowId
     * @param id_food
     * @param food_name
     * @param id_cat
     * @param amount
     * @param desc
     * @param TotalItemPrice
     * @param FoodPrice
     * @param TotalPrice
     */
	public Tobacco(int rowId,int id_food, String food_name, int id_cat, int amount, String desc, 
			Double TotalItemPrice, Double FoodPrice, Double TotalPrice){
		this.rowId = rowId;
		this.id_food = id_food;
		this.food_name = food_name;
		this.id_cat = id_cat;
		this.amount = amount;
		this.desc = desc;
		this.TotalItemPrice = TotalItemPrice;
		this.FoodPrice = FoodPrice;
		this.TotalPrice = TotalPrice;
	}
	
}
