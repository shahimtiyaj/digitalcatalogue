package com.dcastalia.android.digitalcatalogue;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.image.SmartImageView;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Foodmenu is a Login activity where input the table number and waiter id
 */

public class FoodMenu extends Activity implements OnClickListener {

    EditText uname, password;
    Button login_button;
    String user = "TAP TO SELECT";
    String pass = "TAP TO SELECT";
    Global global;
    public OnClickListener uname_click_listener = new OnClickListener() {

        public void onClick(View arg0) {
            uname.setText(user);
            global.setTABLE_ID(pass);
            startActivity(new Intent("TableIDDialog"));
        }

    };
    public OnClickListener password_click_listener = new OnClickListener() {

        public void onClick(View arg0) {
            password.setText(user);
            global.setWAITER_ID(pass);
            startActivity(new Intent("WaiterIDDialog"));
        }

    };
    int width, height;
    Display mDisplay;
    SmartImageView logoView;
    TextView open_time, close_time, r_name, r_desc;
    OptionsParser OP;
    String optionsUrl;
    DataHelper dh;
    public OnClickListener login_listener = new OnClickListener() {

        public void onClick(View v) {
            if ((global.getTABLE_ID().equals(user) || (global.getWAITER_ID()).equals(pass))) {
                Toast.makeText(FoodMenu.this, "cannot be empty", Toast.LENGTH_LONG).show();
            } else {
                startActivity(new Intent("AfterLoginScreen").addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                finish();
            }
        }
    };
    Builder EXIT_DIALOG;
    float rx, ry;
    private AlertDialog alertDialog;

    /**
     * Set the view containers.Initialize the xml contents.Specially design related
     */

    public void setElement() {
        LinearLayout parent_layout = (LinearLayout) findViewById(R.id.parent_layout);
        //parent_layout.setPadding(0, (int) (10 * ry), 0, 0);

        LinearLayout header_layout = (LinearLayout) findViewById(R.id.header_layout);
        /*LinearLayout.LayoutParams header_layout_params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT, (int) (64 * ry));
        header_layout.setLayoutParams(header_layout_params);*/

        logoView = (SmartImageView) findViewById(R.id.ImgView);
        /*LayoutParams logo_layout_params = new LayoutParams((int) (64 * rx), (int) (64 * ry));
        logo_layout_params.setMargins((int) (20 * rx), 0, 0, 0);
        logoView.setLayoutParams(logo_layout_params);*/

        LinearLayout background_layout = (LinearLayout) findViewById(R.id.background_layout);
        //background_layout.setPadding((int) (50 * rx), (int) (30 * ry), (int) (30 * rx), (int) (30 * ry));

        LinearLayout body_layout = (LinearLayout) findViewById(R.id.body_layout);
        //body_layout.setPadding((int) (180 * rx), (int) (20 * ry), 0, 0);

        //body_layout.setGravity(Gravity.CENTER);

        LinearLayout cont_layout = (LinearLayout) findViewById(R.id.container_layout);
        /*LinearLayout.LayoutParams cont_layout_params = new LinearLayout.LayoutParams(
                (int) (400 * rx), LinearLayout.LayoutParams.WRAP_CONTENT);
        cont_layout.setLayoutParams(cont_layout_params);
        cont_layout.setPadding((int) (5 * rx), (int) (10 * ry), (int) (5 * rx), (int) (5 * ry));*/

        LinearLayout inputborderlayout1 = (LinearLayout) findViewById(R.id.input_borderlayout1);
        inputborderlayout1.setOnClickListener(uname_click_listener);
        //inputborderlayout1.setPadding((int) (10 * rx), (int) (10 * ry), (int) (10 * rx), (int) (10 * ry));
        LinearLayout inputborderlayout2 = (LinearLayout) findViewById(R.id.input_borderlayout2);
        inputborderlayout2.setOnClickListener(password_click_listener);
        //inputborderlayout2.setPadding((int) (10 * rx), (int) (10 * ry), (int) (10 * rx), (int) (10 * ry));

        TextView unameText = (TextView) findViewById(R.id.uname_text);
        unameText.setOnClickListener(uname_click_listener);
        //unameText.setTextSize((22f * ry) / global.f);
        TextView passText = (TextView) findViewById(R.id.pass_text);
        passText.setOnClickListener(password_click_listener);
        //passText.setTextSize((22f * ry) / global.f);

        login_button = (Button) findViewById(R.id.login_button);
        login_button.setOnClickListener(login_listener);
        /*login_button.setLayoutParams(new LayoutParams((int) (70 * rx), (int) (30 * ry)));
        login_button.setTextSize((15f * ry) / global.f);*/

        /*TextView powerText = (TextView) findViewById(R.id.power_textView);
        powerText.setTextSize((12f * ry) / global.f);

        ImageView frraView = (ImageView) findViewById(R.id.ferra_imageView);
        LayoutParams ferra_params = new LayoutParams((int) (100 * rx), (int) (50 * ry));
        ferra_params.setMargins(0, 0, (int) (80 * rx), 0);
        frraView.setLayoutParams(ferra_params);*/

        r_name = (TextView) findViewById(R.id.r_name);
        //r_name.setTextSize((32f * ry) / global.f);
        r_desc = (TextView) findViewById(R.id.r_desc);
        //r_desc.setTextSize((12f * ry) / global.f);

        Button settings_button = (Button) findViewById(R.id.settings);
        settings_button.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                final Context context = FoodMenu.this;

                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(context);
                builder.setTitle("System Logout");
                //builder.setIcon(R.drawable.icon);
                builder
                        .setMessage("Are you sure? you want to logged-out from system?");
                builder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("LoginSession", MODE_PRIVATE);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.clear();
                                editor.commit(); // commit changes

                                startActivity(new Intent("LoginActivity").addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                                finish();
                            }
                        });

                builder.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder.show();
            }
        });
        LinearLayout linearLayoutSettingsButton = (LinearLayout) findViewById(R.id.settings_button_linearLayout);
        linearLayoutSettingsButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(FoodMenu.this, "Hello", Toast.LENGTH_LONG).show();

                final Context context = FoodMenu.this;

                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(context);
                builder.setTitle("System Logout");
                //builder.setIcon(R.drawable.icon);
                builder
                        .setMessage("Are you sure? you want to logged-out from system?");
                builder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("LoginSession", MODE_PRIVATE);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.clear();
                                editor.commit(); // commit changes

                                startActivity(new Intent("LoginActivity").addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                                finish();
                            }
                        });

                builder.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder.show();
            }
        });


        LinearLayout linearLayoutCloseButton = (LinearLayout) findViewById(R.id.close_button_linearLayout);
        linearLayoutCloseButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final Context context = FoodMenu.this;

                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(context);
                builder.setTitle("EXIT APPLICATION");
                //builder.setIcon(R.drawable.icon);
                builder
                        .setMessage("Do you really want to exit?");
                builder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                finish();
                            }
                        });

                builder.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder.show();
            }
        });

        Button close_button = (Button) findViewById(R.id.close);
        close_button.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                final Context context = FoodMenu.this;

                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(context);
                builder.setTitle("EXIT APPLICATION");
                //builder.setIcon(R.drawable.icon);
                builder
                        .setMessage("Do you really want to exit?");
                builder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                finish();
                            }
                        });

                builder.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder.show();
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login);

        global = new Global();

        //--Getting session from SharedPreferences and SharedPreferences Check for Login Session.

        SharedPreferences sharedPreferencesLoginSession = getApplicationContext().getSharedPreferences("LoginSession", MODE_PRIVATE);
        if (!sharedPreferencesLoginSession.getBoolean("logged_in", false)) {
            startActivity(new Intent("LoginActivity").addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
            finish();
        } else {
//            global.setOptionsXML("http://" + sharedPreferencesLoginSession.getString("domain", "dcastalia.com") + "/projects/web/" + sharedPreferencesLoginSession.getString("username", "null") + "/file/options.xml");
//            global.setMeal_typeXML("http://" + sharedPreferencesLoginSession.getString("domain", "dcastalia.com") + "/projects/web/" + sharedPreferencesLoginSession.getString("username", "null") + "/file/meal_type.xml");
//            global.setOrder_tableUrl("http://" + sharedPreferencesLoginSession.getString("domain", "dcastalia.com") + "/projects/web/" + sharedPreferencesLoginSession.getString("username", "null") + "/index.php/android/save");
//            global.setForm_tableURL("http://" + sharedPreferencesLoginSession.getString("domain", "dcastalia.com") + "/projects/web/" + sharedPreferencesLoginSession.getString("username", "null") + "/index.php/android/save_client_details");
            global.setOptionsXML("http://" + sharedPreferencesLoginSession.getString("domain", "dcastalia.com") + "/" + sharedPreferencesLoginSession.getString("username", "null") + "/file/options.xml");
            global.setMeal_typeXML("http://" + sharedPreferencesLoginSession.getString("domain", "dcastalia.com") + "/" + sharedPreferencesLoginSession.getString("username", "null") + "/file/meal_type.xml");
            global.setOrder_tableUrl("http://" + sharedPreferencesLoginSession.getString("domain", "dcastalia.com") + "/" + sharedPreferencesLoginSession.getString("username", "null") + "/index.php/android/save");
            global.setForm_tableURL("http://" + sharedPreferencesLoginSession.getString("domain", "dcastalia.com") + "/" + sharedPreferencesLoginSession.getString("username", "null") + "/index.php/android/save_client_details");

        }


        setGlobalRatio();
        setElement();

        alertDialog = new AlertDialog.Builder(this).create();

        uname = (EditText) findViewById(R.id.userName);
        uname.setOnClickListener(uname_click_listener);

        password = (EditText) findViewById(R.id.password);
        password.setOnClickListener(password_click_listener);

        uname.setText(user);
        password.setText(pass);

        dh = new DataHelper(this);
        global.setDatahelper(dh);
        global.getDatahelper().deleteAll();
        global.clearBckt();
        global.setCount(0);
        global.setTABLE_ID(user);
        global.setWAITER_ID(pass);
        global.getTobaccoList().clear();

        //initilizing the loading class and call the execute method
        new Loading().execute();

        //startActivity(new Intent("PostForm").addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        //finish();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            uname.setText(global.getTABLE_ID());
            password.setText(global.getWAITER_ID());

            uname.setKeyListener(null);
            password.setKeyListener(null);
        }
    }

    /**
     * Getting screen ratio ,getting height and width
     */
    public void setGlobalRatio() {
        mDisplay = getWindowManager().getDefaultDisplay();

        width = mDisplay.getWidth();
        height = mDisplay.getHeight();

        //Toast.makeText(this, width+" "+height, 10).show();

        global.setRx(width / 800f);
        global.setRy(height / 480f);

        rx = global.getRx(); //Local RX
        ry = global.getRy(); //Local RY

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int screenDensity = metrics.densityDpi;

        global.f = screenDensity / 160f;
    }

    /**
     * @param url get the option url and fetch the xml file from webservices
     */
    public void OptionsReader(String url) {
        try {

            URL website = new URL(url);

            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();

            OP = new OptionsParser();
            global.setOptionsparser(OP);
            xr.setContentHandler(OP);

            xr.parse(new InputSource(website.openStream()));
            //xr.parse(new InputSource(getResources().openRawResource(id)));
            createFile(url);

        } catch (Exception e) {
            Log.d("TAG", "error! " + e);
        }
    }

    /**
     * @param file it works for offline .getting the xml from file instead of webservices .
     *             getting the data and set the optionParser to Global instance.
     *             accurately it set optionParser.java Model
     */
    public void OptionsReaderOff(File file) {
        try {

            //URL website = new URL(url);

            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();


            OP = new OptionsParser();

            global.setOptionsparser(OP);

            xr.setContentHandler(OP);
            //xr.parse(new InputSource(website.openStream()));
            //xr.parse(new InputSource(getResources().openRawResource(id)));

            InputStream in = new BufferedInputStream(new FileInputStream(file));
            xr.parse(new InputSource(in));

        } catch (Exception e) {
            Log.d("TAG", "error! " + e);
        }
    }

    /**
     * @param url getting the xml file from internet than save into file system
     * @throws IOException Signals that an I/O exception of some sort has occurred. This class is the general class of exceptions produced by failed or interrupted I/O operations.
     */
    public void createFile(String url) throws IOException {
        URL url1 = new URL(url);

        //create the new connection

        HttpURLConnection urlConnection = (HttpURLConnection) url1.openConnection();

        //set up some things on the connection

        urlConnection.setRequestMethod("GET");

        urlConnection.setDoOutput(true);

        //and connect!

        urlConnection.connect();

        //set the path where we want to save the file

        //in this case, going to save it on the root directory of the

        //sd card.
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath());
        dir.mkdirs();


        //File SDCardRoot = new File("/sdcard/"+"Some Folder Name/");

        //create a new file, specifying the path, and the filename

        //which we want to save the file as.

        File file = new File(dir, "options.xml");
        Log.w("myApp", "no network");

        //this will be used to write the downloaded data into the file we created

        FileOutputStream fileOutput = new FileOutputStream(file);

        //this will be used in reading the data from the internet

        InputStream inputStream = urlConnection.getInputStream();

        //this is the total size of the file

        int totalSize = urlConnection.getContentLength();

        //variable to store total downloaded bytes

        int downloadedSize = 0;

        //create a buffer...

        byte[] buffer = new byte[1024];

        int bufferLength = 0; //used to store a temporary size of the buffer

        //now, read through the input buffer and write the contents to the file

        while ((bufferLength = inputStream.read(buffer)) > 0)

        {

            //add the data in the buffer to the file in the file output stream (the file on the sd card

            fileOutput.write(buffer, 0, bufferLength);

            //add up the size so we know how much is downloaded

            downloadedSize += bufferLength;

            int progress = (int) (downloadedSize * 100 / totalSize);

            //this is where you would do something to report the prgress, like this maybe

            //updateProgress(downloadedSize, totalSize);

        }

        //close the output stream when done

        fileOutput.close();
    }

    /**
     * check the internet connection is available or not
     *
     * @return not null
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public boolean isOnline() {
        /*ConnectivityManager cm =
            (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
	        return true;
	    }*/
        return false;
    }

    public void onClick(View v) {

    }

    @Override
    public void finish() {
        super.finish();
        if (dh != null)
            dh.close();
    }

    /**
     * AsyncTask is the inner class .this class get the data from webservices if internet is available
     */
    class Loading extends AsyncTask<String, Void, Void> {
        ProgressDialog dialog = new ProgressDialog(FoodMenu.this);

        /**
         * invoked on the background thread immediately after onPreExecute() finishes executing. This step is used to perform background computation that can take a long time.
         * @param params  The parameters of the asynchronous task are passed to this step.
         * @return
         */

        @Override
        protected Void doInBackground(String... params) {
            if (isNetworkAvailable()) {
                global.isOnline = true;
                try {
                    Random rnd = new Random();
                    optionsUrl = global.getOptionsXML();
                    //+ "?p=" + rnd.nextInt();

                    Log.d("Option URL", optionsUrl);
                    OptionsReader(optionsUrl);

                    //OptionsReaderOff(R.raw.options);

                } catch (Exception e) {
                }
            } else {
                global.isOnline = false;
                //OptionsReaderOff(R.raw.options);

                File sdCard = Environment.getExternalStorageDirectory();
                File dir = new File(sdCard.getAbsolutePath());
                File file = new File(dir, "options.xml");
                OptionsReaderOff(file);

            }
            return null;
        }

        /**
         * Invoked on the UI thread after the background computation finishes.
         */
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
                //startActivity(new Intent("ListCategoryView").addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                //finish();
            }
            try {
                logoView.setImageUrl(OP.restaurent.logo);
                Global.restaurantLogo = OP.restaurent.logo;
                r_name.setText(OP.restaurent.getName());
                r_desc.setText(OP.restaurent.getDesc());
                //open_time.setText("From:  "+OP.restaurent.getOpen());
                //close_time.setText("To:    "+OP.restaurent.getClose());
                global.setVat(Double.parseDouble(OP.commerce.getVat()));
                global.setService(Double.parseDouble(OP.commerce.getService()));
            } catch (Exception e) {
                Log.d("TAG", e.toString());
                alertDialog.setTitle("Connection Error!");
                alertDialog.setMessage("Unable to connect! Please \ncheck your network connection!");
                alertDialog.setButton("EXIT", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //new Intent("FoodMenu");
                        finish();
                        return;
                    }
                });
                //alertDialog.show();
            }
        }

        /**
         * onPreExecute invoked on the UI thread before the task is executed. This step is normally used to setup the task, for instance by showing a progress bar in the user interface.
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.dialog.setMessage("Connecting...");
            this.dialog.show();
        }
    }
}