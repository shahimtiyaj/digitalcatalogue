package com.dcastalia.android.digitalcatalogue;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.image.SmartImageView;

import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;

public class ItemView extends Activity implements OnClickListener, OnTouchListener {
    public OnClickListener MenuText_click_listener = new OnClickListener() {

        public void onClick(View v) {
            finish();
        }
    };
    public OnClickListener Back_Button_Listener = new OnClickListener() {

        public void onClick(View arg0) {
            finish();
        }
    };
    AnimationDrawable adAnimation;
    String[] values;
    int menuid, itemid;
    Global global;
    public OnClickListener checkout_listener = new OnClickListener() {

        public void onClick(View v) {
            try {
                if (global.getCount() > 0 || global.getTobaccoList().size() > 0)
                    startActivity(new Intent("ConfWin"));
                else
                    Toast.makeText(ItemView.this, "Please select an item", Toast.LENGTH_SHORT).show();
            } catch (Exception x) {
                Log.d("TAG", x.toString());
            }

        }
    };
    public OnClickListener categoryText_click_listener = new OnClickListener() {

        public void onClick(View v) {
            global.getMenu_ctx().finish();
            //startActivity(new Intent("ListCategoryView"));
            finish();
        }

    };
    public OnClickListener item_listener = new OnClickListener() {

        public void onClick(View v) {
            global.setFeaturedItemClicked(false);
            global.setItemId(v.getId());
            itemid = global.getItemId();
            startActivity(new Intent("PopupView"));
        }
    };
    Button close;
    Button checkout;
    Button Exit;
    Button Back;
    TextView ItemText;
    MenuParser Menuparser;
    TableLayout bckt_table;
    String unit_price;
    ListCategoryParser Listparser;
    OptionsParser OP;
    public OnClickListener stad_listener = new OnClickListener() {
        public void onClick(View v) {
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(OP.stad.link));
                startActivity(intent);

            } catch (Exception x) {
            }
        }
    };
    int i;
    int scrollX, scrollY;
    Timer mmTimer;
    TextView ItemName;
    TextView ItemDesc;
    TextView catstr;
    ImageView ItemImage;
    String checkvalue[];
    SmartImageView staticImg;
    SmartImageView adview;
    SmartImageView logoView;
    TextView open_time, close_time, r_name, r_desc;
    //ImageView sc_down, sc_up;
    float rx, ry;
    Builder EXIT_DIALOG;
    public OnClickListener EXIT_APPLICATION = new OnClickListener() {

        public void onClick(View v) {
            EXIT_DIALOG
                    .setTitle("EXIT APPLICATION")
                    .setMessage("Do you really want to exit?")
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            try {
                                global.getMenu_ctx().finish();
                                System.runFinalizersOnExit(true);
                                System.exit(0);
                                //android.os.Process.killProcess(android.os.Process.myPid());
                            } catch (Exception e) {

                            }
                        }
                    })
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            //do stuff onclick of YES
                            return;
                            //finish();
                        }
                    }).show();

        }
    };
    ScrollView item_scroller;
    TableLayout itl;
    TableRow itr;
    TableLayout ftl;
    TableRow ftr;
    boolean elementadded;
    DecimalFormat format = new DecimalFormat("###.##");
    String click_text = " ITEMS CHOSEN: ";
    String s = "";
    boolean toggle = true;
    private ListView Lister;
    private AlertDialog alertDialog;
    private TextView categoryText;
    private TextView MenuText;
    private Timer mTimer;
    private long delayInMili = 5000;
    private int nextImg = 0;
    public OnClickListener adview_listener = new OnClickListener() {
        public void onClick(View v) {
            String link = OP.ads.get(((nextImg - 1) % OP.ads.size())).link;
            if (!link.startsWith("http"))
                link = "http://" + link;
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(link));
                startActivity(intent);
            } catch (Exception x) {
            }
        }
    };

    public void setElements() {

        //--------- Main Layout --------------------------------------------------------------------

        LinearLayout main_layout = (LinearLayout) findViewById(R.id.main_layout);
        LinearLayout.LayoutParams main_layout_params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        main_layout_params.setMargins(0, 0, 0, 0);
        main_layout.setLayoutParams(main_layout_params);
        main_layout.setOrientation(LinearLayout.HORIZONTAL);

        //--------- Main Layout Left ---------------------------------------------------------------

        LinearLayout main_layout_left = (LinearLayout) findViewById(R.id.left_main_layout); //FindViewByID

        /*LinearLayout.LayoutParams main_layout_left_params = new LinearLayout.LayoutParams
                ((int) (600 * rx), LinearLayout.LayoutParams.MATCH_PARENT);
        main_layout_left_params.setMargins(0, 0, 0, 0);
        main_layout_left.setLayoutParams(main_layout_left_params);
        main_layout_left.setOrientation(LinearLayout.VERTICAL);*/

        TableRow cat_text_row = (TableRow) findViewById(R.id.breadcrumb_textView_layout); //FindViewByID

        /*TableRow.LayoutParams cat_text_row_params = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, (int) (40 * ry));
        cat_text_row_params.setMargins(0, 0, 0, 0);
        cat_text_row.setLayoutParams(cat_text_row_params);*/

        categoryText = (TextView) this.findViewById(R.id.category_name_textView); //FindViewByID
        categoryText.setOnClickListener(categoryText_click_listener);
        //categoryText.setTextSize((18f * ry) / global.f);

        MenuText = (TextView) this.findViewById(R.id.menu_name_textView); //FindViewByID
        MenuText.setOnClickListener(MenuText_click_listener);
        //MenuText.setTextSize((18f * ry) / global.f);

        ItemText = (TextView) this.findViewById(R.id.item_name_textView); //FindViewByID
        //ItemText.setTextSize((18f * ry) / global.f);

        item_scroller = (ScrollView) findViewById(R.id.item_scrollView); //FindViewByID
        item_scroller.setOnTouchListener(this);
        /*LayoutParams item_scroller_params = new LayoutParams(LayoutParams.MATCH_PARENT,
                (int) (340 * ry));
        item_scroller_params.setMargins(0, 0, 0, 0);
        item_scroller.setLayoutParams(item_scroller_params);
        item_scroller.setVerticalFadingEdgeEnabled(false);*/

        //sc_down = (ImageView) findViewById(R.id.item_scroll_down); //FindViewByID
        //sc_up = (ImageView) findViewById(R.id.item_scroll_up); //FindViewByID
        /*LayoutParams sc_down_params = new LayoutParams(LayoutParams.FILL_PARENT, (int) (10 * ry));
        LayoutParams sc_up_params = new LayoutParams(LayoutParams.FILL_PARENT, (int) (10 * ry));
        sc_down_params.setMargins((int) (15 * rx), 0, 0, 0);
        sc_up_params.setMargins((int) (15 * rx), 0, 0, 0);
        sc_down.setScaleType(ScaleType.FIT_XY);
        sc_up.setScaleType(ScaleType.FIT_XY);
        sc_down.setLayoutParams(sc_down_params);
        sc_up.setLayoutParams(sc_up_params);*/

        //--------- Main Layout Right --------------------------------------------------------------

        LinearLayout main_layout_right = (LinearLayout) findViewById(R.id.right_main_layout); //FindViewByID
        /*LinearLayout.LayoutParams main_layout_right_params = new LinearLayout.LayoutParams
                ((int) (200 * rx), LinearLayout.LayoutParams.MATCH_PARENT);
        //main_layout_right_params.setMargins((int) (5 * rx), (int) (5 * ry), 0, (int) (5 * ry));
        main_layout_right.setLayoutParams(main_layout_right_params);
        main_layout_right.setOrientation(LinearLayout.VERTICAL);

        */
        LinearLayout mcatstr_linearlayout = (LinearLayout) findViewById(R.id.header_linearLayout); //FindViewByID
        /*
        LayoutParams mcatstr_linearlayout_params = new LayoutParams(LayoutParams.MATCH_PARENT, (int) (40 * ry));
        mcatstr_linearlayout.setLayoutParams(mcatstr_linearlayout_params);

        */
        SmartImageView smartImageView = (SmartImageView) findViewById(R.id.ImgView);
        smartImageView.setImageUrl(Global.restaurantLogo);
        /*
        Menucategorystr.setOnClickListener(categoryText_click_listener);
        Menucategorystr.setTextSize((18f * ry) / global.f);

        */
        LinearLayout list_layout = (LinearLayout) findViewById(R.id.menu_list_linearLayout); //FindViewByID
        /*
        LinearLayout.LayoutParams list_layout_params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        list_layout_params.setMargins(0, 0, 0, 0);
        //list_layout_params.setMargins(0, 0, 0, (int)(2*ry));
        list_layout.setLayoutParams(list_layout_params);

        */
        //LinearLayout button_layout = (LinearLayout) findViewById(R.id.back_button_linearLayout); //FindViewByID
        /*
        LinearLayout.LayoutParams button_layout_params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        button_layout_params.setMargins(0, 0, 0, 0);
        //list_layout_params.setMargins(0, 0, 0, (int)(2*ry));
        button_layout.setLayoutParams(list_layout_params);
        button_layout.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        */
        LinearLayout linearLayoutBackButton = (LinearLayout) findViewById(R.id.linearLayout_breadcrumb_back);
        linearLayoutBackButton.setOnClickListener(Back_Button_Listener);

        Back = (Button) findViewById(R.id.back_button); //FindViewByID
        Back.setOnClickListener(Back_Button_Listener);
        /*
        LayoutParams back_params = new LayoutParams(LayoutParams.MATCH_PARENT, (int) ((35 * ry) / global.f));
        back_params.setMargins((int) (1.5 * rx), 0, (int) (1.5 * rx), 0);
        //back_params.setMargins(left, top, right, bottom);
        Back.setLayoutParams(back_params);
        */
        LinearLayout linearLayoutCheckOutButton = (LinearLayout) findViewById(R.id.checkout_button_linearLayout);
        linearLayoutCheckOutButton.setOnClickListener(checkout_listener);

        checkout = (Button) this.findViewById(R.id.checkout_button); //FindViewByID
        checkout.setOnClickListener(checkout_listener);
        /*

        LayoutParams checkout_params = new LayoutParams((int) (90 * rx), (int) (64 * ry));
        checkout_params.setMargins(0, 0, (int) (5 * rx), 0);
        checkout.setLayoutParams(checkout_params);*/

        //--------- Bottom layout --------------------------------------------------------------

		/*LinearLayout bottom_layout = (LinearLayout) findViewById(R.id.bottom_layout);
        LinearLayout.LayoutParams bottom_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, (int) (64 * ry));
		bottom_layout.setLayoutParams(bottom_layout_params);
		bottom_layout.setOrientation(LinearLayout.HORIZONTAL);
		logoView = (SmartImageView) findViewById(R.id.logoview);
		logoView.setLayoutParams(new LayoutParams((int) (64 * rx), (int) (64 * ry)));
		logoView.setScaleType(ScaleType.FIT_XY);

		adview = (SmartImageView) findViewById(R.id.adImageView);
		adview.setOnClickListener(adview_listener);
		adview.setLayoutParams(new LayoutParams((int) (540 * rx), (int) (64 * ry)));
		adview.setScaleType(ScaleType.FIT_XY);

		checkout = (Button) this.findViewById(R.id.Checkout);
		checkout.setOnClickListener(checkout_listener);

		LayoutParams checkout_params = new LayoutParams((int) (90 * rx), (int) (64 * ry));
		checkout_params.setMargins(0, 0, (int) (5 * rx), 0);
		checkout.setLayoutParams(checkout_params);

		Exit = (Button) this.findViewById(R.id.Exit);
		Exit.setOnClickListener(EXIT_APPLICATION);

		LayoutParams exit_params = new LayoutParams((int) (90 * rx), (int) (64 * ry));
		exit_params.setMargins(0, 0, (int) (5 * rx), 0);
		Exit.setLayoutParams(exit_params);
		Exit.setTextSize((16f * (ry - .1f)) / global.f);*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.item);

        global = new Global();

        rx = global.getRx();
        ry = global.getRy();

        s = global.getBckt();

        menuid = global.getMenuId();

        OP = global.getOptionsparser();

        EXIT_DIALOG = new AlertDialog.Builder(this);

        Listparser = global.getListparser();
        Menuparser = global.getMenuparser();

        if ((Menuparser.mlist.get(global.getMenuId()).name).equals("BENSON & HEDGES")) {
            global.getMenu_ctx().finish();
        }
        setElements();
        try {
            //categoryText.setText(OP.restaurent.getName() + " Category ");
            categoryText.setText("Categories");
            MenuText.setText(Global.toCamelCaseSentence(Listparser.list.get(global.getListId()).getName()) + " Items ");
            if ((Menuparser.mlist.get(global.getMenuId()).name).equals("BENSON & HEDGES"))
                ItemText.setText("");
            else
                ItemText.setText(Global.toCamelCaseSentence(Menuparser.mlist.get(menuid).getName()));
            logoView.setImageUrl(OP.restaurent.logo);
            adview.setImageUrl(OP.ads.get((nextImg = nextImg % OP.ads.size())).image);
        } catch (Exception e) {
        }

        values = new String[Listparser.list.size()];

        for (int i = 0; i < values.length; i++) {
            values[i] = Listparser.list.get(i).name;
        }

        Lister = (ListView) findViewById(R.id.menu_listView);

        MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(this, values, Listparser.list.get(global.getListId()).getName());
        Lister.setAdapter(adapter);
        Lister.setOnItemClickListener(new OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                if (global.getListId() != position) {
                    global.setListId(position);
                    global.getMenu_ctx().finish();
                    startActivity(new Intent("MenuView"));
                    finish();
                } else if (!(Menuparser.mlist.get(global.getMenuId()).name).equals("BENSON & HEDGES")) {
                    finish();
                }
            }

        });

        itl = (TableLayout) findViewById(R.id.item_tableLayout);
        ftl = (TableLayout) findViewById(R.id.featured_items_tableLayout);

        if (Menuparser.mlist.get(menuid).item.size() > 0) {

            i = 0;
            elementadded = false;
            String unit_price;
            for (i = 0; i < Menuparser.mlist.get(menuid).item.size(); i++) {
                if (i % 2 == 0) {
                    itr = new TableRow(this);
                    //itr.setGravity(Gravity.CENTER);
                    itr.setPadding((int) (10 * rx), (int) (10 * ry), (int) (10 * rx), (int) (10 * ry));
                    itr.setLayoutParams(new LayoutParams(
                            LayoutParams.FILL_PARENT,
                            LayoutParams.WRAP_CONTENT));
                    elementadded = false;
                }
                unit_price = Menuparser.mlist.get(menuid).item.get(i).unit + "  " +
                        Menuparser.mlist.get(menuid).item.get(i).price;
                itr.addView(ItemButton(Menuparser.mlist.get(menuid).item.get(i).id,
                        Menuparser.mlist.get(menuid).item.get(i).name,
                        Menuparser.mlist.get(menuid).item.get(i).image,
                        Menuparser.mlist.get(menuid).item.get(i).desc,
                        unit_price));
                if (i % 2 == 1) {
                    elementadded = true;
                    itl.addView(itr, new TableLayout.LayoutParams(
                            LayoutParams.FILL_PARENT,
                            LayoutParams.WRAP_CONTENT));
                }
            }
            if (!elementadded)
                itl.addView(itr, new TableLayout.LayoutParams(
                        LayoutParams.FILL_PARENT,
                        LayoutParams.WRAP_CONTENT));
        } else {
            alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Please check your network connection");
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    return;
                }
            });
            alertDialog.show();
        }
        item_scroller.setOnTouchListener(this);
    }

    public LinearLayout ItemButton(String id, String name, String img2, String desc, String unit_price) {

        LinearLayout parent = new LinearLayout(this);
        parent.setBackgroundResource(R.drawable.button_background);
        //parent.setBackgroundColor(Color.parseColor("#ffffff"));
        parent.setOrientation(LinearLayout.HORIZONTAL);
        //parent.setPadding((int) (18 * rx), (int) (12 * ry), (int) (6 * rx), (int) (2 * ry));
        parent.setClickable(true);
        parent.setOnClickListener(item_listener);
        parent.setId(Integer.parseInt(id));

        LayoutParams params = new LayoutParams(
                (int) (250 * rx),
                (int) (80 * ry)
        );

        params.setMargins((int) (10 * rx), 0, (int) (10 * rx), 0);

        parent.setLayoutParams(params);
        /*parent.setLayoutParams(new LayoutParams(
                (int) (270 * rx),
                (int) (100 * ry)));*/


        LinearLayout child1 = new LinearLayout(this);
        SmartImageView imgView = new SmartImageView(this);
        imgView.setImageUrl(img2);
        if ((Menuparser.mlist.get(global.getMenuId()).name).equals("BENSON & HEDGES"))
            imgView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        else
            imgView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        imgView.setScaleType(ScaleType.CENTER_CROP);
        child1.setLayoutParams(new LayoutParams(
                0, LayoutParams.MATCH_PARENT, 2f));
        child1.addView(imgView);
        //child1.setPadding((int) (0 * rx), (int) (0 * ry), (int) (10 * rx), (int) (0 * ry));

        parent.addView(child1);

        LinearLayout child2 = new LinearLayout(this);
        child2.setOrientation(LinearLayout.VERTICAL);
        child2.setLayoutParams(new LayoutParams(
                0,
                LayoutParams.MATCH_PARENT, 4f));

        child2.setPadding((int) (10 * rx), (int) (10 * ry), (int) (0 * rx), (int) (0 * ry));

        TextView text1 = new TextView(this);
        text1.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (10f * ry), 2f));
        text1.setTextSize((12f * ry) / global.f);
        text1.setTextColor(android.graphics.Color.BLACK);
        text1.setTypeface(null, android.graphics.Typeface.BOLD);
        text1.setText(Global.toCamelCaseSentence(name));

        //------------------------ Cut String ---------------------
        String inputString = desc;
        String cutString;
        if (inputString.length() > 50) {
            cutString = inputString.substring(0, 50);
            desc = cutString + "...";
        }
        //------------------------ Cut String ---------------------

        TextView text2 = new TextView(this);
        text2.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 4f));
        text2.setText(desc);
        text2.setTextColor(Color.BLACK);
        text2.setTypeface(null, Typeface.NORMAL);
        text2.setTextSize((10f * ry) / global.f);

        /*LinearLayout child3 = new LinearLayout(this);
        //child3.setBackgroundResource(R.drawable.add_2_bckt);
        LinearLayout.LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1f);
        child3.setLayoutParams(lp);
        //lp.setMargins(0, (int) (4 * ry), 0, 0);

        LinearLayout child4 = new LinearLayout(this);
        child4.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));*/

        //TextView size ,color and padding by programmatically
        TextView text3 = new TextView(this);
        text3.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (10f * ry), 2f));
        text3.setPadding((int) (0 * rx), (int) (0 * ry), (int) (5 * rx), (int) (5 * ry));
        text3.setText(unit_price);
        text3.setTextColor(Color.BLACK);
        text3.setGravity(Gravity.RIGHT);
        text3.setTypeface(null, android.graphics.Typeface.BOLD);
        text3.setTextSize((12f * ry) / global.f);

        child2.addView(text1);
        child2.addView(text2);
        child2.addView(text3);
        parent.addView(child2);

        return parent;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //		global.getMenu_ctx().finish();
        finish();
    }

    /*
     * 		s+=global.getBckt();
        global.setBckt(s);
        t.setText("");
        if(!(s.equals("")))
            t.setText(click_text+s);
     */
    public void onClick(View v) {
        //Log.d("TAG","clicked!");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				adview.post(new Runnable() {
					public void run() {
						adview.setImageUrl(OP.ads.get((nextImg = nextImg%OP.ads.size())).image);
						nextImg+=1;
					}
				});
			}
		},0,delayInMili);*/
        mmTimer = new Timer();
        mmTimer.schedule(new TimerTask() {
            @Override
            public void run() {

                item_scroller.post(new Runnable() {
                    public void run() {
                        try {
                            if (item_scroller != null) {
                                scrollX = item_scroller.getScrollX();
                                scrollY = item_scroller.getScrollY();
                            }
                            if (itl != null && itl.getChildCount() > 3) {
                                if (scrollY == 0) {
                                    //sc_up.setImageResource(R.drawable.arrow_background_1);
                                    //sc_down.setImageResource(R.drawable.arrow_down_1);

                                }
                                //250 is width of one element, there r 3 elements 10 extra space
                                else if ((scrollY + (int) (100 * ry * 3) + 50 * ry) > itl.getHeight()) {
                                    //sc_up.setImageResource(R.drawable.arrow_up_1);
                                    //sc_down.setImageResource(R.drawable.arrow_background_1);
                                }
                            }
                        } catch (Exception e) {

                        }

                    }
                });
            }
        }, 0, 100);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //		Log.v("ON_TOUCH", "Touched");
        return super.onTouchEvent(event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        //		Log.v("ON_TOUCH", "Action = " + ev.getAction());
        return super.dispatchTouchEvent(ev);
    }

    public boolean onTouch(View v, MotionEvent event) {
        //		int action = event.getAction();
        float x = event.getX();
        float y = event.getY();
        if (itl != null && itl.getChildCount() > 3) {
            if (scrollY == 0) {
                //sc_up.setImageResource(R.drawable.arrow_background_1);
                //sc_down.setImageResource(R.drawable.arrow_down_1);

            }
            //250 is width of one element, there r 3 elements 10 extra space
            else if ((scrollY + (int) (100 * ry * 3) + 50 * ry) >= itl.getHeight()) {
                //sc_up.setImageResource(R.drawable.arrow_up_1);
                //sc_down.setImageResource(R.drawable.arrow_background_1);
            } else {
                //sc_up.setImageResource(R.drawable.arrow_up_1);
                //sc_down.setImageResource(R.drawable.arrow_down_1);
            }
        }
        //		if(itl!=null&&itl.getChildCount()>3&&itl.getChildCount()<=6){
        //			if((v.getBottom()/2-y)>0){
        //				sc_down.setImageResource(R.drawable.arrow_background);
        //				sc_up.setImageResource(R.drawable.arrow_up_1);
        //			}
        //			else{
        //				sc_down.setImageResource(R.drawable.arrow_down_1);
        //				sc_up.setImageResource(R.drawable.arrow_background);
        //			}
        //		}
        //		if(itl!=null&&itl.getChildCount()>6){
        //			sc_up.setImageResource(R.drawable.arrow_up_1);
        //			sc_down.setImageResource(R.drawable.arrow_down_1);
        //		}
        //	    Log.v("ON_TOUCH", "Action = " + action + " View:" + v.toString());
        //	    Log.v("ON_TOUCH", "X = " + x + "Y = " + y+"Y_AXIS = "+Y_AXIS);
        //	    Log.v("ON_TOUCH", (v.getBottom()/2-y)+"");
        //	    Log.v("ON_TOUCH", itl.getBottom()+" is TABLE BOTTOM");
        //	    Toast.makeText(ItemView.this, v.getId()+"", 1).show();
        return false;
    }

    //----------------------------------------------------------------------------------------------


}